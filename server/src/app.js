const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const User = require("../models/User");

app.use(cors());
app.use(bodyParser.json());

const validate = (requestData) => {
  let localErrors = [];
  if (!requestData || Object.keys(requestData).length === 0) {
    localErrors.push("Bad data");
    return localErrors;
  }

  if (!requestData.firstName || requestData.firstName.length === 0) {
    localErrors.push("Bad Request: firstName");
  }

  if (!requestData.lastName || requestData.lastName.length === 0) {
    localErrors.push("Bad Request:  lastName");
  }

  if (!requestData.email || requestData.email.length === 0) {
    localErrors.push("Bad Request:  email");
  } else if (!/\S+@\S+\.\S+/.test(requestData.email)) {
    localErrors.push("Bad Request: email");
  }

  if (!requestData.date || requestData.date.length === 0) {
    localErrors.push("Bad Request: date");
  } else if (!/(\d{4})+\-(\d{2})+\-(\d{2})/.test(requestData.date)) {
    localErrors.push("Bad Request: date");
  }

  return localErrors;
};

app.put("/user", async (req, res) => {
  const errors = validate(req.body);
  if (errors.length) {
    res.status(400).send({ data: errors });
  } else {
    const user = new User({ ...req.body });
    try {
      const ans = await user.save();
      console.log(ans);
    } catch (e) {
      console.log(e);
      res.status(500).send("problem with db");
    }

    res.status(200).send("user added");
  }
});

module.exports = app;
