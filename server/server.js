const app = require("./src/app");
const port = 3001;
const mongoose = require("mongoose");

const connString = process.env.MONGODB_CONNSTRING
  ? process.env.MONGODB_CONNSTRING
  : process.argv[2];

if (connString) {
  mongoose.connect(connString);
} else {
  return;
}

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
