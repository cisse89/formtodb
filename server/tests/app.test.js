const request = require("supertest");
require("jest");

var sinon = require("sinon");
let User = require("../models/User");

const app = require("../src/app");

describe("Test the root path", () => {
  test("It should response with status 200 the PUT method when fields are correct", async () => {
    sinon.mock(User.prototype).expects("save").returns("added");

    const response = await request(app).put("/user").send({
      email: "john.doe@site.com",
      date: "2022-10-06",
      firstName: "John",
      lastName: "Doe",
    });

    expect(response.statusCode).toBe(200);
  });

  test("It should response with status 400 the PUT method when fields are not correct", async () => {
    const response = await request(app).put("/user");
    expect(response.statusCode).toBe(400);
  });

  test("It should response with status 400 the PUT method when firstName not provided", async () => {
    const response = await request(app).put("/user").send({
      email: "john.doe@site.com",
      date: "2022-10-06",
      lastName: "Doe",
    });

    expect(response.statusCode).toBe(400);
  });

  test("It should response with status 400 the PUT method when firstName empty", async () => {
    const response = await request(app).put("/user").send({
      email: "john.doe@site.com",
      date: "2022-10-06",
      lastName: "Doe",
      firstName: "",
    });

    expect(response.statusCode).toBe(400);
  });

  test("It should response with status 400 the PUT method when lastName not provided", async () => {
    const response = await request(app).put("/user").send({
      email: "john.doe@site.com",
      date: "2022-10-06",
      firstName: "John",
    });

    expect(response.statusCode).toBe(400);
  });

  test("It should response with status 400 the PUT method when lastName empty", async () => {
    const response = await request(app).put("/user").send({
      email: "john.doe@site.com",
      date: "2022-10-06",
      lastName: "",
      firstName: "John",
    });

    expect(response.statusCode).toBe(400);
  });

  test("It should response with status 400 the PUT method when email not provided", async () => {
    const response = await request(app).put("/user").send({
      date: "2022-10-06",
      firstName: "John",
      lastName: "Doe",
    });

    expect(response.statusCode).toBe(400);
  });

  test("It should response with status 400 the PUT method when email empty", async () => {
    const response = await request(app).put("/user").send({
      email: "",
      date: "2022-10-06",
      lastName: "Doe",
      firstName: "John",
    });

    expect(response.statusCode).toBe(400);
  });

  test("It should response with status 400 the PUT method when date not provided", async () => {
    const response = await request(app).put("/user").send({
      firstName: "John",
      lastName: "Doe",
      email: "john.doe@site.com",
    });

    expect(response.statusCode).toBe(400);
  });

  test("It should response with status 400 the PUT method when date empty", async () => {
    const response = await request(app).put("/user").send({
      email: "john.doe@site.com",
      date: "",
      lastName: "Doe",
      firstName: "John",
    });

    expect(response.statusCode).toBe(400);
  });
});
