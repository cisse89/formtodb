const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const schema = new Schema({
  firstName: String,
  lastName: String,
  email: String,
  date: String,
});

module.exports = mongoose.model("User", schema);
