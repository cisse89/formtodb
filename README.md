An application introducing communication between React, Express server and MongoDB databae

Client application contains a simople form written in react togetehr with button to add user into the database

Server application listens for a put method containing user data


## RUN APP
Install docker-compose to run the application with single command
1. Run command `docker-compose up --build`
2. Open `localhost:3000` in your browser

## Running the tests
There are unit tests written for both client and server side, to run them go to interested directory and run:

1. `npm install`
2. `npm test`


## Run app outside the docker
There is also possibility to run application outside the docker container.
Server accepts MongoDB address as first parameter, there is localStart script prepared to use default docker mongoDB address

1. `docker run --name mongodb -d -p 27017:27017 mongo`
2. `cd server && npm localStart`
3. `cd client && npm start`
