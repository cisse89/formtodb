import React, { ChangeEvent, ReactNode, useEffect, useState } from "react";
import db from "../apis/db";
import { FormType } from "../types/FormTypes.types";

const FORM_INIT = {
  firstName: "",
  lastName: "",
  email: "",
  date: "",
};

const SimpleForm: React.FC<{}> = () => {
  const [form, setForm] = useState<FormType>(FORM_INIT);

  const [errors, setErrors] = useState<string[]>();
  const [response, setResponse] = useState<string>("");
  const [isValidating, setIsValidating] = useState<boolean>(false);
  const [isMagSent, setIsMagSent] = useState<boolean>(false);

  const onButtonClicked = async () => {
    console.log(form);

    setIsValidating(true);

    if ((!isValidating && validate().length) || errors?.length) return;

    try {
      console.log("sending data");
      setIsMagSent(true);
      let res = await db.put("/user", { ...form });
      setResponse(res.data);
    } catch (e: any) {
      console.log(e);
      setResponse(e.message);
    }
  };

  useEffect(() => {
    if (isValidating) {
      validate();
    }
  }, [form]);

  const onFormChaged = (event: ChangeEvent<HTMLInputElement>) => {
    setForm({ ...form, [event.currentTarget.id]: event.currentTarget.value });
  };

  const validate = () => {
    console.log("validate");
    let localErrors: string[] = [];
    if (form.firstName.length === 0) {
      localErrors.push("Enter firstName");
    }

    if (form.lastName.length === 0) {
      localErrors.push("Enter lastName");
    }

    if (form.email.length === 0) {
      localErrors.push("Enter email");
    } else if (!/\S+@\S+\.\S+/.test(form.email)) {
      localErrors.push("Enter valid email");
    }

    if (form.date.length === 0) {
      localErrors.push("choose date");
    }

    console.log(localErrors);
    setErrors(localErrors);
    return localErrors;
  };

  const renderErrors = () => {
    return (
      <div>
        {errors?.map((err, id) => {
          return (
            <div key={`err-id-${id}`}>
              <span style={{ color: "red" }}>{err}</span>
            </div>
          );
        })}
      </div>
    );
  };

  const renderForm = () => {
    return (
      <div>
        <div>
          <label>FirstName: </label>
          <input
            aria-label="firstName"
            id="firstName"
            type="text"
            value={form.firstName}
            onChange={onFormChaged}
          />
        </div>
        <div>
          <label>LastName: </label>
          <input
            aria-label="lastName"
            id="lastName"
            value={form.lastName}
            onChange={onFormChaged}
          ></input>
        </div>
        <div>
          <label>Email: </label>
          <input
            aria-label="email"
            id="email"
            value={form.email}
            onChange={onFormChaged}
          />
        </div>
        <div>
          <label>Date: </label>
          <input
            aria-label="date"
            id="date"
            type="date"
            value={form.date}
            onChange={onFormChaged}
          ></input>
        </div>
        <div>{renderErrors()}</div>

        <button id="submitButton" onClick={onButtonClicked}>
          SUBMIT
        </button>
      </div>
    );
  };

  const renderResp = () => {
    return (
      <div id="serverResp">
        {response}{" "}
        <button
          id="newUser"
          onClick={() => {
            setIsValidating(false);
            setForm(FORM_INIT);
            setIsMagSent(false);
            setResponse("");
          }}
        >
          New user
        </button>
      </div>
    );
  };

  return !isMagSent ? <div>{renderForm()}</div> : <div>{renderResp()}</div>;
};

export default SimpleForm;
