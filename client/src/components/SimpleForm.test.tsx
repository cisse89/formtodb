import mockAxios from "jest-mock-axios";

import React from "react";
import { fireEvent, render } from "@testing-library/react";

import SimpleForm from "./SimpleForm";
import userEvent from "@testing-library/user-event";
import { act } from "react-dom/test-utils";

describe("SimpleForm tests", () => {
  afterEach(() => {
    mockAxios.reset();
  });

  it("put should no be called when fields are empty", async () => {
    const testedComponent = render(<SimpleForm />);
    act(() => {
      fireEvent.click(testedComponent.getByText("SUBMIT"));
    });

    expect(mockAxios.put).not.toBeCalled();
  });

  it("submit should call put method when fields are correct", async () => {
    const testedComponent = render(<SimpleForm />);

    mockAxios.put.mockResolvedValue({ data: "added", status: 200 });

    act(() => {
      userEvent.type(testedComponent.getByLabelText("firstName"), "John");
      userEvent.type(testedComponent.getByLabelText("lastName"), "Doe");
      userEvent.type(
        testedComponent.getByLabelText("email"),
        "john.doe@site.com"
      );
      userEvent.type(testedComponent.getByLabelText("date"), "2022-10-06");
      fireEvent.click(testedComponent.getByText("SUBMIT"));
    });

    expect(mockAxios.put).toHaveBeenCalledWith("/user", {
      email: "john.doe@site.com",
      date: "2022-10-06",
      firstName: "John",
      lastName: "Doe",
    });

    await testedComponent.findByText("New user");
    await testedComponent.findByText("added");
  });

  describe("fields validation", () => {
    it("empty first name should show an error", async () => {
      const testedComponent = render(<SimpleForm />);
      act(() => {
        fireEvent.click(testedComponent.getByText("SUBMIT"));
      });
      expect(testedComponent.findAllByText("Enter firstName")).not.toBe(null);
    });

    it("filled first name should not show an error", async () => {
      const testedComponent = render(<SimpleForm />);
      act(() => {
        userEvent.type(testedComponent.getByLabelText("firstName"), "John");
        fireEvent.click(testedComponent.getByText("SUBMIT"));
      });
      expect(testedComponent.queryByText("Enter firstName")).toBe(null);
    });

    it("empty last name should show an error", async () => {
      const testedComponent = render(<SimpleForm />);
      act(() => {
        fireEvent.click(testedComponent.getByText("SUBMIT"));
      });

      expect(testedComponent.findAllByText("Enter lastName")).not.toBe(null);
    });

    it("filled last name should not show an error", async () => {
      const testedComponent = render(<SimpleForm />);
      act(() => {
        userEvent.type(testedComponent.getByLabelText("lastName"), "John");
        fireEvent.click(testedComponent.getByText("SUBMIT"));
      });
      expect(testedComponent.queryByText("Enter lastName")).toBe(null);
    });

    it("empty email should show an error", async () => {
      const testedComponent = render(<SimpleForm />);
      act(() => {
        fireEvent.click(testedComponent.getByText("SUBMIT"));
      });

      expect(testedComponent.findAllByText("Enter email")).not.toBe(null);
    });

    it("filled correctly email should not show an error", async () => {
      const testedComponent = render(<SimpleForm />);
      act(() => {
        userEvent.type(
          testedComponent.getByLabelText("email"),
          "john.doe@site.com"
        );
        fireEvent.click(testedComponent.getByText("SUBMIT"));
      });
      expect(testedComponent.queryByText("Enter email")).toBe(null);
      expect(testedComponent.queryByText("Enter valid email")).toBe(null);
    });

    it("email filled bad should show an error", async () => {
      const testedComponent = render(<SimpleForm />);
      act(() => {
        userEvent.type(testedComponent.getByLabelText("email"), "@site.com");
        fireEvent.click(testedComponent.getByText("SUBMIT"));
      });
      expect(testedComponent.queryByText("Enter email")).toBe(null);
      expect(testedComponent.queryByText("Enter valid email")).not.toBe(null);
    });
  });
});
