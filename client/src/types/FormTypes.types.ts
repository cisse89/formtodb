export type FormType = {
  firstName: string;
  lastName: string;
  email: string;
  date: string;
};
